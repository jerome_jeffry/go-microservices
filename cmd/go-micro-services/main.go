package main

import (
	"log"
	"os"

	Deviceservice "github.com/Jerome-0609/microservice/device"
	Userservice "github.com/Jerome-0609/microservice/user"
	"google.golang.org/grpc"
)

type server interface {
	Run()
}

//user service runs on 4041
//device service runs on 4040

func main() {

	var (
		useraddr string = "localhost:4041"
		//deviceaddr string ="localhost:4040"
	)
	var srv server
	switch os.Args[1] {
	case "user":
		srv = Userservice.NewUser()
	case "device":
		srv = Deviceservice.NewDevice(getclient(useraddr))
	default:
		log.Fatalf("unknown command %s", os.Args[1])

	}

	srv.Run()
}

func getclient(addr string) *grpc.ClientConn {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return conn
}
