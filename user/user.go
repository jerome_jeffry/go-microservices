package Userservice

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/Jerome-0609/microservice/proto/user"

	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

type User struct {
}

func NewUser() *User {
	return &User{}
}

//user microservice sends Usernames to device micoservice on calling Fetch()
func (us *User) FetchData(ctx context.Context, req *user.Request) (res *user.Result, err error) {

	if err := contextError(ctx); err != nil {
		return nil, err
	}
	userid := req.GetUserId()
	//user service have usernames (db/Inmemory)
	//for time being hardcoded
	fmt.Println("fetching username for userid:", userid)

	//returning the response
	res = &user.Result{
		UserName: "Netobjex",
	}
	return res, nil

}

//checking context errors
func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}

//starting Grpc server on 4041
func (us *User) Run() {

	listener, err := net.Listen("tcp", ":4041")
	if err != nil {
		panic(err)
	}
	fmt.Println("user service started")
	srv := grpc.NewServer()
	user.RegisterUserServer(srv, us)
	reflection.Register(srv)

	if e := srv.Serve(listener); e != nil {
		panic(e)
	}

}
