# go-microservices

This simple code has 2 simple microservices(user service and device service) and a client that will communicate via grpc.  

Code workflow:

1)client calls Device microservice with device id to retrieve device and its user information.
2)Device microservice inturn calls User microservice with userid to retrieve username.
3)Device microservice sends back deviceid, uerid, username to the client.

Run from cmd/go-microservices/main.go with command line args.

steps:

1)go run main.go user ->To run user microservice
2)go run main.go device->To run device microservice
3)client/main.go -> To run a client
